package INF102.lab3.sumList;


import java.util.List;

public class SumRecursive implements ISum {

    @Override
    public long sum(List<Long> list) {
        return sumRecursive(list, 0);
    }

    private long sumRecursive(List<Long> list, int index) {
        if (index >= list.size()) { //smart måte å stoppe på siste element i en liste
            return 0; //siden listen er slutt så plusser man med 0. Tenk stack?
        }
       
        long currentElement = list.get(index);
        long accumulatedSum = sumRecursive(list, index + 1); //esentsielt at jeg fårstår akkumuleringen av infårmasjon i en lokal variabel... må skrive med Å pga testen.
        long sumOfList = currentElement + accumulatedSum; // nyttig å sette breakepoint here og kjøre en og en. Er det riktig å tenke LIFO?
        return sumOfList;
    }
}








