package INF102.lab3.peakElement;

import java.util.List;

public class PeakRecursive implements IPeak {



@Override
public int peakElement(List<Integer> numbers) {

    if (numbers.isEmpty()){
        throw new IllegalArgumentException("Empty list");

        //tre base caser
    }
    if (numbers.size() == 1){
        return numbers.get(0);
    }


    if (numbers.get(0) >= numbers.get(1)){
        return numbers.get(0);

    }
    if (numbers.get(numbers.size() - 1) >= numbers.get(numbers.size() - 2)){
        return numbers.get(numbers.size() - 1);

    }

    return findPeak(numbers, 0, numbers.size() - 1);
}

private int findPeak(List<Integer> numbers, int start, int stop) {

    int midIndex = (start + stop)/2;  //start og stopp er grensene 

    int mid = numbers.get(midIndex);        // finne mid rekursivt

    

    if ((mid >= numbers.get(midIndex - 1)) && (mid >= numbers.get(midIndex + 1))){ //her sjekker jeg om mid er peak

        return mid;
    }

    if (mid > numbers.get(midIndex - 1)){
        start = mid;
    }
    if (mid > numbers.get(midIndex + 1)){
        stop = mid;
    }
    

    return findPeak(numbers, start, stop);
}


}
