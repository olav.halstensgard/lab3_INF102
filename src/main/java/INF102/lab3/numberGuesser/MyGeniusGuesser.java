package INF102.lab3.numberGuesser;

import java.util.Random;

public class MyGeniusGuesser implements IGuesser {

    
	private int lowerbound;
	private int upperbound;

	@Override
    public int findNumber(RandomNumber number) {

        lowerbound = number.getLowerbound();
	    upperbound = number.getUpperbound();

        
        return reNewGuess(number, lowerbound, upperbound);
    }

    private int reNewGuess(RandomNumber number, int lowerbound, int upperbound) {

        int mid = (lowerbound + upperbound)/2;
        int result = number.guess(mid);
        
        while (result != 0){
            //System.out.println(result);

            if (result == -1){

                lowerbound = mid;
                mid = (lowerbound + upperbound)/2;
                result = number.guess(mid);

            }

            if (result == 1){

                upperbound = mid;
                mid = (lowerbound + upperbound)/2;
                result = number.guess(mid);
            }
        }
   
        return mid;
    }

}
